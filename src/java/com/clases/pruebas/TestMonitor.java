/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clases.pruebas;

import com.clases.Monitor;
import java.util.Scanner;

/**
 *
 * @author Alumno
 */
public class TestMonitor {
    
  public static void main(String... arg){
        Monitor monitor = null;
        Scanner sc = new Scanner(System.in);
        String numerodeserie;
        String modelo;
        String marca;
        String color;
        double tamaño;
        String op="";
        System.out.println("CAPTURA DE DATOS DE UN MONITOR");
        while(true){
           System.out.print("Dame el numero de serie: ");
           numerodeserie = sc.nextLine();
           System.out.print("Dame la marca del monitor: ");
           marca = sc.nextLine();
           System.out.print("Dame el modelo del monitor: ");
           modelo = sc.nextLine();
           System.out.print("Dame el color del monitor: ");
           color = sc.nextLine();
           System.out.print("Dame el tamaño de la pantalla: ");
           tamaño = Double.parseDouble(sc.nextLine().trim());
           monitor = new Monitor(numerodeserie, marca,modelo, color,tamaño);
           System.out.println("Los datos que diste son:");
           System.out.println(monitor.toString());
           System.out.println("Para salir teclea <salir>, da <enter> para continuar...");
           op = sc.nextLine().trim().toLowerCase();
           if(op.equals("salir"))
               break;
        }
    }
}

