package com.classes;

import java.util.Objects;

public class Teclado {
    static int n =0;
    private int id;
    private String numeroDeSerie;
    private String marca;
    private String modelo;
    private String color;
    private String numerodeparte;
    public Teclado(String a,String b, String c,String d,String e){
       this.numeroDeSerie = a;
       this.marca = b;
       this.modelo = c;
       this.color = d;
       this.numerodeparte = e;
       this.id = n;
       n++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroDeSerie() {
        return numeroDeSerie;
    }

    public void setNumeroDeSerie(String numeroDeSerie) {
        this.numeroDeSerie = numeroDeSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getnumerodeparte() {
        return numerodeparte;
    }

    public void setnumerodeparte(String numerodeparte) {
        this.numerodeparte = numerodeparte;
    }
    
    @Override
    public String toString(){
       return this.id+","+this.numeroDeSerie+","+this.marca+","+
               this.modelo+","+this.color+","+this.numerodeparte;
    }


    @Override
    public boolean equals(Object other){
         Teclado teclado = (Teclado)other;
         return teclado.getNumeroDeSerie().equals(this.numeroDeSerie) &&
         teclado.getModelo().equals(this.modelo) &&
                 teclado.getMarca().equals(this.marca);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.numeroDeSerie);
        hash = 29 * hash + Objects.hashCode(this.marca);
        hash = 29 * hash + Objects.hashCode(this.modelo);
        hash = 29 * hash + Objects.hashCode(this.color);
        hash = 29 * hash + Objects.hashCode(this.numerodeparte);
        return hash;
    }
}